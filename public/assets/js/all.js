function doLogin() {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    handleLogin(username, password);
}

function getUrlParam(n) {
    var half = location.search.split(n + '=')[1];
    return half !== undefined ? decodeURIComponent(half.split('&')[0]) : null;
}

document.addEventListener("DOMContentLoaded", function () {

    document.getElementById("year").innerHTML = new Date().getFullYear();//set the current year into the copyright text

    var failed = getUrlParam('failed');
    if (failed) {
        document.querySelector('.error-msg').classList.remove('hidden'); //Display a message if login failed
    }
});