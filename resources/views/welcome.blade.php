
<!DOCTYPE html>
<!-- saved from url=(0050)https://trial.brightspace.com/d2l/loginh/?logout=1 -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>
    <!-- Bootstrap core CSS -->
    <link href="./assets/css/bootstrap.css" rel="stylesheet">
    <link href="./assets/css/common.css" rel="stylesheet">
    <link href="./assets/css/style.css" rel="stylesheet">

    <script async="" src="./assets/js/adrum-ext.js"></script>
    <script src="./assets/js/HostedLoginHelpers.js"></script><style></style><script>

</script>
<script async="async" onload="addD2LDataToAdrum()" src="./assets/js/HostedLoginHelpers.js"></script>

    <script src="./assets/js/all.js"></script>
    <style>



    </style>
<link type="image/x-icon" rel="icon" href=""></head>

<body style="background-color: #262626;">


<header id="masthead" role="banner">
    <div id="nav-container" class="row">
        <div class="container">
            <div class="col-lg-8 col-md-offset-2">
            <a href="/"><img style="height:60px;" src="assets/img/logo.png" alt=""></a>
            </div>
        </div>
    </div>
</header>

<div class="greyBG" style="border-bottom: 1px solid #cccccc;min-height:470px;">
    <div class="container padY">

        <div class="row">
            <div class="col-lg-8 col-md-offset-2">
                <h1>Welcome to our SUBMIT HOME</h1>               
            </div>
        </div>
                
        <form method="POST" class="form-horizontal" role="form" action="{{ route('login') }}">
                @csrf
            <span class="hidden error-msg">*  Please try again.</span>

            <div class="row">
                <div class="col-lg-8 col-md-offset-2">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="username" class="control-label center-block"><span>Username:</span><span class="req"><span class="sr-only"> (Required)</span>*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">                           
                            <input id="username" placeholder="Username" type="email" maxlength="255" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="password" class="control-label center-block"><span>Password:</span><span class="req"><span class="sr-only"> (Required)</span>*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">                            
                            <input id="password" type="password" maxlength="255" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password" autocomplete="off">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-offset-2">
                    <div class="col-md-6 col-md-offset-4">
                        <input id="submitButton" class="btn-lg shortcode_btn d2lOrange_bg" type="submit" name="Login" value="LOGIN">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row padY">
                <div class="col-lg-8 col-md-offset-2">
                   
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
      

    </div>
</div>

<div class="whiteBG">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-offset-2">
                <div class="col-lg-4 padY">
                    <img src="./assets/img/icon-1.png" class="text-left center-block" alt=" " height="100" width="100">
                </div>
                <div class="col-lg-4 padY">
                    <img src="./assets/img/icon-2.png" class="text-left center-block" alt=" " height="100" width="100">
                </div>
                <div class="col-lg-4 padY">
                    <img src="./assets/img/icon-3.png" class="text-left center-block" alt=" " height="100" width="100">
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<footer>
    <div class="container padY">
        <div class="row">
            <div class="col-lg-8 col-md-offset-2">
                <div class="col-lg-12">
                   
                </div>
            </div>
        </div>
    </div>
</footer>




</body>
</html>