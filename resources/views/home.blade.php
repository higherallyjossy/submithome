@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="submitcard">
                <div class="card_body">
                    <div class="card_title">
                        <h1 class="maintitle">ANDREW BLACK</h2>
                        <P class="subtitle">SALES MANAGER</P>
                    </div>
                    <div class="card_detail">
                        <p><span class="infotype">M:</span>&nbsp;&nbsp;<span>0417562336</span></p>
                        <p><span class="infotype">D:</span>&nbsp;&nbsp;<span>08 9317 0102</span></p>
                        <p><span class="infotype">E:</span>&nbsp;&nbsp;<span>andrew@summithomesgroup.com.au</span></p>
                    </div>
                </div>
                
                <div class="card_footer">
                    <ul>
                        <li class="right_border text-right">
                            <p><span class="infotype">A:</span><span>242 Leach Hwy, Myaree WA 6154</span></p>
                        </li>
                        <li>
                            <p><span class="infotype">W:</span><span>summithomes.com.au</span></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
